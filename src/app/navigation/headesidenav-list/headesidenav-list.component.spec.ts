import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadesidenavListComponent } from './headesidenav-list.component';

describe('HeadesidenavListComponent', () => {
  let component: HeadesidenavListComponent;
  let fixture: ComponentFixture<HeadesidenavListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadesidenavListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadesidenavListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
