import { Component, OnInit ,EventEmitter,Output, OnDestroy} from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import {Subscription}from 'rxjs/Subscription'
@Component({
  selector: 'app-headesidenav-list',
  templateUrl: './headesidenav-list.component.html',
  styleUrls: ['./headesidenav-list.component.css']
})
export class HeadesidenavListComponent implements OnInit,OnDestroy {
@Output() closeSidenav=new EventEmitter<void>();
isAuth=false
authSubscription:Subscription | undefined;
  constructor(private authService:AuthService) { }

  ngOnInit(): void {
    this.authSubscription= this.authService.authChange.subscribe(authStatus=>{
      this.isAuth=authStatus

    });
  }

  close(){
    this.closeSidenav.emit()
  }
  ngOnDestroy(){
this.authSubscription?.unsubscribe()
  }
  onLogout(){
    this.authService.logout();
    this.close()
  }
}
