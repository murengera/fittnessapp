import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule}from "@angular/material/button";
import {MatIconModule}from "@angular/material/icon";
import { MatInputModule }from "@angular/material/input";
import { MatFormFieldModule } from '@angular/material/form-field';
import{MatSelectModule} from "@angular/material/select"
import {MatAutocompleteModule}from "@angular/material/autocomplete"
import { from } from 'rxjs';
import { MatNativeDateModule}from"@angular/material/core"

import {MatRadioModule}from "@angular/material/radio";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatButtonToggleModule}from "@angular/material/button-toggle";
import{MatTableModule}from "@angular/material/table";
import {MatChipsModule}from"@angular/material/chips"
import {MatSortModule}from "@angular/material/sort";
import{MatProgressSpinnerModule}from "@angular/material/progress-spinner";
import {MatPaginatorModule}from "@angular/material/paginator";
import {MatTabsModule} from '@angular/material/tabs';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule}from '@angular/material/list'
import {MatToolbarModule}from '@angular/material/toolbar'
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule}from '@angular/material/dialog';

import {MatDatepickerModule} from '@angular/material/datepicker';
const MaterialComponents=[MatNativeDateModule,MatDialogModule,MatCardModule,MatTabsModule,MatListModule,MatToolbarModule,MatSidenavModule,MatProgressSpinnerModule,MatSortModule,MatPaginatorModule,MatTableModule,MatChipsModule,MatButtonToggleModule,MatDatepickerModule,MatButtonModule,MatIconModule
,MatInputModule,MatToolbarModule,MatFormFieldModule,MatSelectModule,MatAutocompleteModule]
@NgModule({

  imports: [MaterialComponents],
  exports:[MaterialComponents]
})
export class MaterialModule { }
