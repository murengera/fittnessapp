import { Component, OnDestroy, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Exercise } from '../exercise.model';
import { Observable, Subscription } from 'rxjs';
import 'rxjs/add/operator/map';
import { TrainingService } from '../training.service';

import { map } from 'rxjs/operators';
@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit ,OnDestroy{

  excercises: Exercise[]=[]
  exerciseSubscription!: Subscription;
  constructor(private trainingService:TrainingService ) { }

  ngOnInit() {
    this.exerciseSubscription=this.trainingService.exercisedChanged.subscribe(exercises=>{
    this.excercises=exercises 
    });
    this.trainingService.fetchAVailableExercise();
   
  }

  onstartTraining(form:NgForm){
this.trainingService.startExercise(form.value.exercise)
  }
  ngOnDestroy(){
    this.exerciseSubscription.unsubscribe();

  }
}
