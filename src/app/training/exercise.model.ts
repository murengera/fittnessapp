export interface Exercise{
  id?:any;
  name?:string;
  duration?:number;
  calories:number;
  date:Date;
  state?:'completed'| 'cancalled' | null;
    
}