import { Injectable } from '@angular/core';
import { Exercise } from './exercise.model';
import { Subject } from 'rxjs/Subject';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class TrainingService {
  exerciseChanged: any = new Subject<Exercise>();
  availableExrcises: Exercise[] = [];
  exercisedChanged = new Subject<Exercise[]>();
  finishedExercisesChanged=new Subject<Exercise[]>();
  runningExercise?: any=[];
  finishedExercieses: Exercise[] = [];
  constructor(private db: AngularFirestore) {}

  startExercise(selectedId:string){

    this.runningExercise=this.availableExrcises.find(
      ex=>ex.id==selectedId
    );
    this.exerciseChanged.next({...this.runningExercise})


  }
  // startExercise(selectedId: string) {
    // this.runningExercise ==
      // this.availableExrcises.find((ex) => {
        // ex.id === selectedId;
      // });
    // this.exerciseChanged.next({ ...this.runningExercise });
  // }

  getRunningExercise() {
    return { ...this.runningExercise };
  }

  completeExerices() {
    this.addDataToDatabase({
      ...this.runningExercise,
      date: new Date(),
      state: 'completed',
    });
    this.runningExercise = null;
    this.exerciseChanged.next(null);

    this.exerciseChanged.next(null);
  }
  cancelExercise(progress: number) {
    this.addDataToDatabase({
      ...this.runningExercise,
      date: new Date(),
      state: 'cancalled',
      duration: this.runningExercise.duration * (progress / 100),
      calories: this.runningExercise.duration * (progress / 100),
    });
    this.runningExercise = null;
    this.exerciseChanged.next(null);
  }
  fetchgetCompletedOrCancelledExercise() {
  this.db.collection('finishedExercises').valueChanges().subscribe((exercises:any)=>{

    this.finishedExercisesChanged.next(exercises)

  });
  }
  fetchAVailableExercise() {
    this.db
      .collection('availableExercises')
      .valueChanges()
      .subscribe((exercises: any) => {
        console.log(exercises);
        this.availableExrcises = exercises;
        this.exercisedChanged.next([...this.availableExrcises]);
      });
  }

  private addDataToDatabase(exercise:Exercise){
    this.db.collection('finishedExercises').add(exercise)

  }
}
