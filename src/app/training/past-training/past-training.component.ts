import { AfterViewInit, Component, OnDestroy, OnInit,ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { Exercise } from '../exercise.model';
import { TrainingService } from '../training.service';

@Component({
  selector: 'app-past-training',
  templateUrl: './past-training.component.html',
  styleUrls: ['./past-training.component.css']
})
export class PastTrainingComponent implements OnInit,AfterViewInit,OnDestroy {
  displayedColumns=['date','name','duration','calories','state']
  dataSource=new MatTableDataSource<Exercise>();
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;
 exChangedSubscription!: Subscription;
  constructor(private trainingService:TrainingService) { }

  ngOnInit(): void {
   this.exChangedSubscription= this.trainingService.finishedExercisesChanged.subscribe((exercise:Exercise[])=>{
      this.dataSource.data=exercise
    })
   this.trainingService.fetchgetCompletedOrCancelledExercise()
  }
  ngAfterViewInit(){
    this.dataSource.sort=this.sort

  }

  ngOnDestroy(){
this.exChangedSubscription.unsubscribe()
  }
}
